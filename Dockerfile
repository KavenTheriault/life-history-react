FROM node:19.2.0 as builder

COPY . /app
WORKDIR /app

RUN npm install
RUN npm run build

FROM abiosoft/caddy:latest

COPY --from=builder /app/build/ /srv
ADD Caddyfile /etc/Caddyfile
