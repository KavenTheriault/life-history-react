import dateFormat from 'dateformat';

export const formatDate = (date: Date) => dateFormat(date, 'yyyy-mm-dd');
export const formatTime = (timeString: string) => `${timeString}:00`;

export const parseDate = (dateString: string) =>
  new Date(dateString + 'T00:00:00');
