import { faSpinner } from '@fortawesome/free-solid-svg-icons';
import React from 'react';
import { SpinnerIcon } from './styles';

export const Spinner = () => <SpinnerIcon icon={faSpinner} />;
