import styled from 'styled-components';
import { GAP_SMALL } from '../../../../common/styles';

export const EditActivityContainer = styled.div`
  display: flex;
  flex-direction: column;
  gap: ${GAP_SMALL};
`;
